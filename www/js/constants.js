/**
 * Created by kHaLiL on 10/03/2016.
 */
var parkly = parkly || {};
parkly.modules = parkly.modules || {};
parkly.modules.app = parkly.modules.app || {};
parkly.modules.app

		/* Conf Local */
		/*.constant('APIBaseURL', 'http://192.168.1.10:81/api/v1')
		.constant('BASEURL', 'http://192.168.1.10:81')*/

		/* Conf Homol */
		.constant('APIBaseURL', 'http://www.parkly.tk/api/v1')
		.constant('BASEURL', 'http://www.parkly.tk')

		/* Conf Prod */
		/*.constant('APIBaseURL', 'http://app.parkly.fr/api/v1')
		.constant('BASEURL', 'http://app.parkly.fr')*/

		.constant('defaultLocation', {
			latitude: 48.8566140,
			longitude: 2.3522219
		})

		.constant('$ionicLoadingConfig', {
			template: '<div class="spinner-logo"></div>'
		})

		.constant('pushAPIToken', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIzNWZiZjJmMS1kYWYwLTRiMDctOGY2Mi00NDk5N2Q0MGZmMGIifQ.bZ0kHGObaNw0M8BgVsNnP--XpHQudCL6JbmRjz_Z_rE');
