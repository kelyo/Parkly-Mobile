/**
 * Created by kHaLiL on 30/04/2016.
 */
// Récupération de l'application existante ou création si nécessaire
var parkly = parkly || {};
// Récupération des modules de l'application existante ou création si nécessaire
parkly.modules = parkly.modules || {};
// Récupération du module controller de l'application existante ou création si nécessaire
parkly.modules.controllers = parkly.modules.controllers || angular.module('parkly.controllers', []);

// Création du controleur pour la vue Login
parkly.modules.controllers
		.controller('BrowserController', ['$scope', '$state', '$rootScope','$log', '$ionicLoading', '$http',
			function ($scope, $state, $rootScope, $log, $ionicLoading, $http) {

				var _self = this;

				$scope.$on('$ionicView.beforeEnter', function() {
					$rootScope.viewTitle = $state.params.title;
					$rootScope.showBackButton = $state.params.allowBack || false;
				});

				$scope.$on('$ionicView.enter', function() {
					_self.loadURL();
				});

				_self.loadURL = function(){
					$scope.errorLoadingMessage = null;

					$ionicLoading.show({
						noBackdrop: true,
						hideOnStateChange: true
					});

					if($rootScope.networkAvailable) {
						$http.get($state.params.url).then(function (data) {
							var doc = document.getElementById('browser').contentWindow.document;
							doc.open();
							doc.write(data.data);
							doc.close();
							$ionicLoading.hide();
						}, function (error) {
							$log.warn('Browser : ', error);
							$scope.errorLoadingMessage = 'Erreur de chargement de la page. Vérifiez que vous êtes bien connecté, puis réessayez';
							$ionicLoading.hide();
						});
					} else{
						$ionicLoading.hide();
						$rootScope.Utils.showNetworkErrorMsg();
						$rootScope.Utils.goBack();
					}
				};
			}
		]);