// Récupération de l'application existante ou création si nécessaire
var parkly = parkly || {};
// Récupération des modules de l'application existante ou création si nécessaire
parkly.modules = parkly.modules || {};
// Récupération du module controller de l'application existante ou création si nécessaire
parkly.modules.controllers = parkly.modules.controllers || angular.module('parkly.controllers', []);

// Création du controleur pour la vue Login
parkly.modules.controllers
		.controller('AppController', ['$state', '$timeout', '$rootScope', '$localStorage', '$auth', '$ionicPlatform', '$interval', '$log',
			'$nlFramework', '$http', 'APIBaseURL', 'AuthenticationService', 'NotificationService', '$nlDrawer',
			function ($state, $timeout, $rootScope, $localStorage, $auth, $ionicPlatform, $interval, $log,
					  $nlFramework, $http, APIBaseURL, AuthenticationService, NotificationService, $nlDrawer) {

				$log.debug('AppCtrl');

				$rootScope.currentUser = $localStorage.getObject('user', null);

				// TODO déplacer $rootScope.currentUserStars dans un controlleur pour le menu (seul endroit où il est utilisé
				// TODO sortir cette variable du $rootScope
				$rootScope.currentUserStars = {
					iconOn: 'ion-ios-star',
					iconOff: 'ion-ios-star-outline',
					iconOnColor: '#333',
					iconOffColor: '#333',
					rating: $rootScope.currentUser ? Math.round($rootScope.currentUser.street_nb_etoiles * 2) / 2 : 0,
					minRating: 0,
					readOnly:true
				};

				// Register device for push notification
				if(window.cordova) {
					NotificationService.register();
				}

				$rootScope.toggleMenu = function(){
					$nlDrawer.toggle();
				};

				/***************************************************************
				 * NLFramework
				 * Native drawer
				 * Initialisation dans AppCtrl pour ne pas impacter les vues Welcome et Login
				 *
				 ***************************************************************/


				$ionicPlatform.ready(function(){
					// assign parts for better usage
					$rootScope.fw = $nlFramework;
					$rootScope.drawer = $nlFramework.drawer;
					//$rootScope.refresh = $nlFramework.refresh;
					$rootScope.burger = $nlFramework.burger;
					$rootScope.config = $nlFramework.config;
					//$rootScope.toast = $nlFramework.toast;
					// show me config
					$log.debug( $rootScope.config );

					// initialize the whole framework
					// Options
					//
					var nlOptions = {
						// global settings
						speed: 0.2,
						animation: 'ease',
						// use toast messages
						toast: false,
						// burger specific
						burger: {
							use: false,
							//use: true,
							endY: 6,
							reverse: false,
							startScale: 1, // X scale of bottom and top line of burger menu at starting point (OFF state)
							endScale: 0.7 // X scale of bottom and top line of burger menu at end point (ON state)
						},
						// content specific
						content:{
							topBarHeight: 0,
							//topBarHeight: 56,
							modify: true
						},
						// drawer specific
						drawer: {
							maxWidth: 300,
							openCb: function(){
								//$log.info('%c[≡]%c $nlDrawer: opened', 'color: #333;', 'color: #558844;');
								$rootScope.map.setClickable(false);
							},
							closeCb: function(){
								//$log.info('%c[≡]%c $nlDrawer: closed', 'color: #333;', 'color: #558844;');
								$rootScope.map.setClickable(true);
							}
						},
						// refresh specific
						refresh: false,
						secMenu: false
					};
					// initialize the framework
					$nlFramework.init( nlOptions );
				});

				$rootScope.rateApp = function(){
					AppRate.preferences = {
						openStoreInApp: true,
						displayAppName: 'Parkly',
						usesUntilPrompt: 2,
						promptAgainForEachNewVersion: false,
						useCustomRateDialog: true,
						callbacks: {
							onRateDialogShow: function(callback){
								callback(1); // cause immediate click on 'Rate Now' button
								$log.debug('onRateDialogShow => ', callback);
							},
							onButtonClicked: function(buttonIndex){
								console.log("onButtonClicked -> " + buttonIndex);
							}
						},
						storeAppURL: {
							//ios: '<my_app_id>',
							android: 'market://details?id=fr.parkly.parklyMobile',
							//windows: 'ms-windows-store://pdp/?ProductId=<the apps Store ID>',
							//blackberry: 'appworld://content/[App Id]/',
							//windows8: 'ms-windows-store:Review?name=<the Package Family Name of the application>'
						},
						customLocale: {
							title: "Noter Parkly",
							message: "Si vous appreciez Parkly, merci de prendre un instant pour noter l'application. Cela ne prendra pas plus d'une minute. Merci de votre soutien!",
							cancelButtonLabel: "Non, Merci",
							laterButtonLabel: "Plus tard",
							rateButtonLabel: "Noter"
						}
					};

					AppRate.promptForRating();
				};

				$rootScope.shareApp = function(){
					var options = {
						message: 'Je me gare facile avec #Parkly. Essaye toi aussi !', // not supported on some apps (Facebook, Instagram)
						subject: 'Découvre l\'application Parkly', // fi. for email
						//files: ['', ''], // an array of filenames either locally or remotely
						url: 'http://app.parkly.fr',
						chooserTitle: 'Partager via...' // Android only, you can override the default share sheet title
					};

					var onSuccess = function(result) {
						$log.debug("Share result", result);
						$log.debug("Share completed? ", result.completed); // On Android apps mostly return false even while it's true
						$log.debug("Shared to app: ", result.app); // On Android result.app is currently empty. On iOS it's empty when sharing is cancelled (result.completed=false)
					};

					var onError = function(msg) {
						$log.warn("Sharing failed with message: ", msg);
					};

					window.plugins.socialsharing.shareWithOptions(options, onSuccess, onError);
				};
			}
		]);
