/**
 * Created by kHaLiL on 13/03/2016.
 */
// Récupération de l'application existante ou création si nécessaire
var parkly = parkly || {};
// Récupération des modules de l'application existante ou création si nécessaire
parkly.modules = parkly.modules || {};
// Récupération du module controller de l'application existante ou création si nécessaire
parkly.modules.services = parkly.modules.services || angular.module('parkly.services', []);

// Création du controleur pour la vue Login
parkly.modules.services
		.factory('HttpResponseInterceptor', ['$localStorage', function ($localStorage) {
			var HttpResponseInterceptor = {};

			HttpResponseInterceptor.response = function(response){
				var serverTime = parseInt(response.headers('time')) || (new Date().getTime() / 1000);
				$localStorage.set('time-offset', Math.round(serverTime - (new Date().getTime() / 1000)));
				return response;
			};

			return HttpResponseInterceptor;
		}])


		.config(['$httpProvider', function($httpProvider) {
			$httpProvider.interceptors.push('HttpResponseInterceptor');
		}]);
