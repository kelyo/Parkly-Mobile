/**
 * Created by kHaLiL on 14/03/2016.
 */
// Récupération de l'application existante ou création si nécessaire
var parkly = parkly || {};
// Récupération des modules de l'application existante ou création si nécessaire
parkly.modules = parkly.modules || {};
// Récupération du module controller de l'application existante ou création si nécessaire
parkly.modules.services = parkly.modules.services || angular.module('parkly.services', []);

// Création du controleur pour la vue Login
parkly.modules.services
		.factory('geoLocation', ['$localStorage', 'defaultLocation', function ($localStorage, defaultLocation) {
			return {
				setGeolocation: function (latitude, longitude) {
					var position = {
						latitude: latitude,
						longitude: longitude
					};
					$localStorage.setObject('userLocation', position);
				},
				getGeolocation: function () {
					var location = $localStorage.getObject('userLocation', defaultLocation);
					return {
						lat: location.latitude,
						lng: location.longitude
					};
				}
			};
		}]);
