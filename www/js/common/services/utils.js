/**
 * Created by kHaLiL on 12/11/2016.
 */
var parkly = parkly || {};
parkly.modules = parkly.modules || {};
parkly.modules.services = parkly.modules.services || angular.module('parkly.services', []);
parkly.modules.services
		.factory('Utils', ['$log', 'APIBaseURL', '$ionicHistory', '$localStorage', '$cordovaDialogs', '$rootScope', '$http', '$q',
			function ($log, APIBaseURL, $ionicHistory, $localStorage, $cordovaDialogs, $rootScope, $http, $q) {
				var Utils = {};

				/**
				 * Met à jour la liste des couleurs depuis le serveur
				 * @returns {Promise}
				 */
				Utils.updateColors = function(){
					var deferred = $q.defer();

					if($rootScope.networkAvailable) {
						$http.get(APIBaseURL + '/cars/couleurs').then(function (response) {
							var couleurs = response.data.couleurs;
							$localStorage.setObject('couleurs', response.data.couleurs);

							var formattedColors = {};
							for(var i=0;i<couleurs.length;i++){
								formattedColors[couleurs[i].color_id] = couleurs[i].color_name;
							}
							$localStorage.setObject('couleursFormatted', formattedColors);
							deferred.resolve(formattedColors);
						}, function(){
							deferred.reject();
						});
					} else{
						deferred.reject();
					}

					return deferred.promise;
				};

				Utils.goToMaps = function(lat, lng){
					//window.open('geo:'+lat+','+lng);
					launchnavigator.navigate([lat, lng]);
				};

				Utils.goBack = function(){
					$ionicHistory.goBack();
				};

				Utils.showNetworkErrorMsg = function(){
					this.showToast('Connexion internet indisponible. Veuillez vous connecter et réessayer.', 'Connexion indisponible');
				};

				Utils.showToast = function(message, title){
					message = message || null;

					if(!message) {return;}

					title = title || '';
					window.plugins.toast.showLongBottom(message, function (a) {

					}, function (error) {
						$cordovaDialogs.alert(message, title);
					});
				};


				return Utils;
			}
		]);
