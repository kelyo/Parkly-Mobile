/**
 * Created by kHaLiL on 12/11/2016.
 */
var parkly = parkly || {};
parkly.modules = parkly.modules || {};
parkly.modules.filters = parkly.modules.filters || angular.module('parkly.filters', []);

parkly.modules.filters
		.filter('avatar', ['BASEURL', function (BASEURL) {
			return function (avatar) {
				if(!avatar){
					return 'img/avatar.jpg';
				}

				if(avatar.substring(0, 7) !== 'http://' && avatar.substring(0, 8) !== 'https://' && avatar.substring(0, 7) !== 'file://') {
					avatar = BASEURL + avatar;
				}

				if(avatar.indexOf('rsz.io') !== -1){
					return avatar+'?w=120&h=120&mode=crop';
				}

				return avatar;
			};
		}]);