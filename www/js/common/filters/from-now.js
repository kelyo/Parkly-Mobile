/**
 * Created by kHaLiL on 11/11/2016.
 * Retourne le temps restant jusqu'au dateTime reçu en entrée, en format text (ex: dans 4 minutes)
 */
var parkly = parkly || {};
parkly.modules = parkly.modules || {};
parkly.modules.filters = parkly.modules.filters || angular.module('parkly.filters', []);

parkly.modules.filters
		.filter('fromNow', ['$localStorage', function($localStorage) {
			return function (time) {
				var offset = $localStorage.getInt('time-offset', 0);
				return moment((time - offset) * 1000).fromNow();
			};
		}]);