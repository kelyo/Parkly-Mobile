/**
 * Created by kHaLiL on 30/04/2016.
 */
// Récupération de l'application existante ou création si nécessaire
var parkly = parkly || {};
// Récupération des modules de l'application existante ou création si nécessaire
parkly.modules = parkly.modules || {};
// Récupération du module controller de l'application existante ou création si nécessaire
parkly.modules.controllers = parkly.modules.controllers || angular.module('parkly.controllers', []);

// Création du controleur pour la vue Login
parkly.modules.controllers
		.controller('ProfileController', ['$scope', '$state', '$ionicPopup', '$log', '$rootScope', '$http', 'APIBaseURL', 'UserService',
			'$localStorage', '$ionicScrollDelegate', '$ionicActionSheet', '$filter', function ($scope, $state, $ionicPopup, $log, $rootScope, $http, APIBaseURL, UserService,
												   $localStorage, $ionicScrollDelegate, $ionicActionSheet, $filter) {

				var _self = this;
				$scope.data = {};

				$scope.marques = $localStorage.getObject('marques', []);
				$scope.modeles = $localStorage.getObject('modeles', []);

				$scope.uploadPercentage = 0;
				$scope.uploading = false;

				$scope.$on('$ionicView.beforeEnter', function() {
					$rootScope.viewTitle = 'Mon profil';
				});

				$scope.$on('$ionicView.enter', function() {
					_self.init();
				});

				$scope.userRating = {
					iconOn: 'ion-ios-star',
					iconOff: 'ion-ios-star-outline',
					iconOnColor: 'rgb(255, 255, 255)',
					iconOffColor: 'rgb(255, 255, 255)',
					rating: Math.round($rootScope.currentUser.street_nb_etoiles * 2) / 2,
					minRating: 0,
					readOnly:true
				};

				_self.init = function(){

					var date = new Date();
					var current = date.getFullYear();
					var start = current - 18;  // Minus 10 years from current year
					var end = start - 100;  // Plus 10 years to current year
					$scope.yearArray = [];

					for(var i=start;i>=end;i--)
					{
						$scope.yearArray.push(i);
					}
				};


				// Parallax
				$scope.onUserDetailContentScroll = function(){
					var scrollDelegate = $ionicScrollDelegate.$getByHandle('userDetailContent');
					var scrollView = scrollDelegate.getScrollView();
					$scope.$broadcast('userDetailContent.scroll', scrollView);
				};

				$scope.editName = function(){
					$ionicPopup.show({
						template: '' +
						'<label class="item item-input item-floating-label" style="border:0;">'+
						'<span class="input-label"">Prénom</span>'+
						'<input type="text" ng-model="currentUser.first_name" placeholder="Prénom" class="text-dark">'+
						'</label>'+
						'<label class="item item-input item-floating-label" style="border:0;">'+
						'<span class="input-label">Nom de famille</span>'+
						'<input type="text" ng-model="currentUser.last_name" placeholder="Nom de famille" class="text-dark">'+
						'</label>',
						scope: $scope,
						title: 'Mon profil',
						cssClass: 'profile-popup',
						buttons: [
							{
								text: 'Annuler',
								type: 'button-stable button-outline',
							},
							{
								text: '<b>Enregistrer</b>',
								type: 'button-energized',
								onTap: function() {
									if($rootScope.networkAvailable) {
										UserService.editUser($rootScope.currentUser);
									} else{
										$rootScope.Utils.showNetworkErrorMsg();
									}
								}
							}
						]
					});
				};

				$scope.profileChanged = function(e){
					if($rootScope.networkAvailable) {
						$log.info('Profile : couleurChanged()');
						UserService.editUser($rootScope.currentUser);
					} else{
						$rootScope.Utils.showNetworkErrorMsg();
						e.preventDefault();
					}
				};

				$scope.showPhotoChangeActionsheet = function() {
					if(!$scope.uploading) {
						$ionicActionSheet.show({
							titleText: 'Changer la photo de profil',
							buttons: [
								{text: '<i class="icon ion-camera"></i> Depuis l\'appareil photo'},
								{text: '<i class="icon ion-image"></i> Depuis la galerie'},
							],
							cancelText: 'Annuler',
							cancel: function () {
								$log.debug('CANCELLED');
							},
							buttonClicked: function (index) {
								$log.debug('BUTTON CLICKED', index);
								if (index === 0) {
									$scope.getPicture('camera');
								} else if (index === 1) {
									$scope.getPicture('gallery');
								}
								return true;
							}
						});
					}
				};

				$scope.getPicture = function(source){
					var pictureSource = source === 'camera' ? navigator.camera.PictureSourceType.CAMERA : navigator.camera.PictureSourceType.PHOTOLIBRARY;
					navigator.camera.getPicture(
							function(imageURI){

								var ft = new FileTransfer(),
										options = new FileUploadOptions();

								options.fileKey = 'avatar';
								options.fileName = imageURI.substr(imageURI.lastIndexOf('/') + 1);
								options.mimeType = 'image/jpeg';
								options.chunkedMode = false;
								options.headers = {
									'Authorization': 'Bearer ' + $localStorage.get('satellizer_token')
								};
								options.params = {};

								ft.onprogress = function(progressEvent) {
									if (progressEvent.lengthComputable) {
										$scope.$apply(function(){
											$scope.uploadPercentage = Math.floor(progressEvent.loaded / progressEvent.total * 100);
										});
									} else {
										$scope.$apply(function() {
											$scope.uploadPercentage++;
										});
									}
								};

								if($rootScope.networkAvailable){
									$scope.uploading = true;
									ft.upload(imageURI, encodeURI(APIBaseURL + '/users/' + $rootScope.currentUser.id + '/avatar'),
											function (e) {
												var avatarURL = $filter('avatar')(JSON.parse(e.response).url) + '?w=120&h=120&mode=crop&decache=' + Math.random();

												$scope.$apply(function(){
													$rootScope.currentUser.avatar = JSON.parse(e.response).url;
													$scope.uploading = false;
													$scope.uploadPercentage = 0;
												});
											},
											function (e) {
												$log.error('upload error ', e);
												$rootScope.Utils.showToast('Une erreur est survenue lors du changement de la photo de profil. Vérifiez que vous connecté puis réessayez', 'Erreur');
												$scope.$apply(function() {
													$scope.uploading = false;
													$scope.uploadPercentage = 0;
												});
											}, options);
								} else{
									$rootScope.Utils.showNetworkErrorMsg();
								}
							}, function(message) {
								$log.debug('get picture failed', message);
							},{ // options
								quality: 50,
								targetWidth: 720,
								//targetHeight: 512,
								encodingType : navigator.camera.EncodingType.JPEG,
								correctOrientation: true,
								destinationType: navigator.camera.DestinationType.FILE_URI,
								sourceType: pictureSource
							}
					);
				};

				$scope.goToAddCar = function(){
					$state.go('user-car', {'allowBack': true});
				};
			}
		]);