/**
 * Created by kHaLiL on 13/03/2016.
 */
// Récupération de l'application existante ou création si nécessaire
var parkly = parkly || {};
// Récupération des modules de l'application existante ou création si nécessaire
parkly.modules = parkly.modules || {};
// Récupération du module controller de l'application existante ou création si nécessaire
parkly.modules.services = parkly.modules.services || angular.module('parkly.services', []);

// Création du controleur pour la vue Login
parkly.modules.services
		.factory('UserService', ['$log', '$http', 'APIBaseURL', function ($log, $http, APIBaseURL) {
			var UserService = {};
			
			UserService.editUser = function(userDetails){
				$log.debug('UserService.editUser()');
				$log.debug('UserService : ', userDetails);

				return $http.put(APIBaseURL + '/users/'+userDetails.id, {
					user : userDetails
				});
			};
			
			
			UserService.getUserCar = function(userId){
				$log.debug('UserService.getUserCar('+userId+')');

				return $http.get(APIBaseURL + '/users/' + userId + '/car');
			};


			

			
			return UserService;
		}]);
