/**
 * Created by kHaLiL on 07/04/2016.
 */

var parkly = parkly || {};
parkly.modules = parkly.modules || {};
parkly.modules.services = parkly.modules.services || angular.module('parkly.services', []);

// Création du service
parkly.modules.services
		.service('HistoriqueService', ['$http', '$q', '$rootScope', 'APIBaseURL', '$log', function ($http, $q, $rootScope, APIBaseURL, $log) {
			var _self = this;

			_self.historySpots = [];

			_self.getSpots = function () {
				var deferred = $q.defer();

				if(_self.historySpots.length > 0){
					deferred.resolve(_self.historySpots);
				} else{
					$http.get(APIBaseURL + '/users/'+$rootScope.currentUser.id+'/history').then(function(response){
						$log.debug('$get history data : ', response);
						_self.historySpots = response.data.historique;
						deferred.resolve(response.data.historique);
					}, function(){
						deferred.reject('Un problème est survenu lors de la récupération de l\'historique');
					});
				}

				return deferred.promise;
			};

			_self.refresh = function(){
				return $http.get(APIBaseURL + '/users/'+$rootScope.currentUser.id+'/history');
			};

		}]);