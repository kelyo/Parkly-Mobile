/**
 * Created by kHaLiL on 30/04/2016.
 */
var parkly = parkly || {};
parkly.modules = parkly.modules || {};
parkly.modules.controllers = parkly.modules.controllers || angular.module('parkly.controllers', []);
parkly.modules.controllers
		.controller('StreetDemandesReceivedController', ['$rootScope', '$scope', '$state', 'NotificationsService', '$log', '$ionicHistory', '$localStorage',
			'$ionicModal', 'APIBaseURL', '$http',
			function ($rootScope, $scope, $state, NotificationsService, $log, $ionicHistory, $localStorage, $ionicModal, APIBaseURL, $http) {
				$scope.demandes = $localStorage.getObject('demandesRecues', []);
				$scope.notation = {};

				$scope.$on('$ionicView.beforeEnter', function(){
					$rootScope.viewTitle = 'Demandes reçues';

					$scope.notation.userRating = {
						iconOn: 'ion-ios-star',
						iconOff: 'ion-ios-star-outline',
						iconOnColor: '#f1611a',
						iconOffColor: 'rgb(0, 0, 0)',
						rating: 0,
						minRating: 1,
						readOnly: false, //Optional
						callback: function (rating) {
							$scope.notation.note = rating;
						}
					};
				});

				$scope.$on('$ionicView.afterEnter', function(event, data){
					$scope.demandesLoaded = false;

					if($rootScope.networkAvailable) {
						NotificationsService.getDemandes().then(function(response){
							$scope.demandes = response.data.filter(function (reservation) {
								return reservation.spot !== null;
							});
							for(var i=0; i<$scope.demandes.length;i++){
								$scope.demandes[i].userRating = {
									iconOn: 'ion-ios-star',
									iconOff: 'ion-ios-star-outline',
									iconOnColor: 'rgb(249, 223, 99)',
									iconHalfColor: 'rgb(249, 223, 99)',
									iconOffColor: 'rgb(0, 0, 0)',
									rating: Math.round($scope.demandes[i].demander.street_nb_etoiles * 2) / 2,
									minRating: 0,
									readOnly:true
								};
							}
							$scope.demandesLoaded = true;

							$scope.nextLink = response.next_page_url;
							$localStorage.setObject('demandesRecues', data);
							$log.debug('$scope.demandes = ', $scope.demandes);
						});
					} else{
						$scope.demandesLoaded = true;
						$rootScope.Utils.showNetworkErrorMsg();
					}
				});

				$scope.refreshNotifications = function(){
					if($rootScope.networkAvailable) {
						NotificationsService.getDemandes().then(function(response){
							$scope.demandes = response.data.filter(function(reservation){
								return reservation.spot !== null;
							});
							for(var i=0; i<$scope.demandes.length;i++){
								$scope.demandes[i].userRating = {
									iconOn: 'ion-ios-star',
									iconOff: 'ion-ios-star-outline',
									iconOnColor: 'rgb(249, 223, 99)',
									iconHalfColor: 'rgb(249, 223, 99)',
									iconOffColor: 'rgb(0, 0, 0)',
									rating: Math.round($scope.demandes[i].demander.street_nb_etoiles * 2) / 2,
									minRating: 0,
									readOnly:true
								};
							}
							$scope.nextLink = response.next_page_url;
							$localStorage.setObject('demandesRecues', $scope.demandes);
							$scope.demandesLoaded = true;
						}).finally(function(){
							$scope.$broadcast('scroll.refreshComplete');
						});
					} else{
						$scope.demandesLoaded = true;
						$rootScope.Utils.showNetworkErrorMsg();
						$scope.$broadcast('scroll.refreshComplete');
					}
				};

				$scope.goToHome = function(){
					$ionicHistory.nextViewOptions({
						disableBack: true
					});

					$state.go('app.je-cherche');
				};

				$scope.showNotationModal = function(index){
					$scope.index = index;
					$scope.notation.user = $scope.demandes[index].demander;
					$scope.notation.note = 0;

					$ionicModal.fromTemplateUrl('street/modal-notation.html', {
						id: 'modalNotation',
						scope: $scope,
						animation: 'slide-in-up',
						backdropClickToClose: true
					}).then(function (modal) {
						$scope.modalNotation = modal;
						$scope.modalNotation.show();
					});

					$scope.$on('modalNotation.hidden', function () {
						$scope.modalNotation.remove();
					});
				};

				$scope.noter = function(index, note){
					if(!index || !note) {return;}

					if($rootScope.networkAvailable){
						$http.post(APIBaseURL + '/bookings/'+$scope.demandes[index].id+'/note',{
							note: note
						}).then(function(response){
							$scope.demandes[index] = response.data.reservation;
							$localStorage.setObject('demandesEnvoyees', $scope.demandes);
							$rootScope.Utils.showToast('Note bien prise en compte, merci', 'Merci');
							$scope.modalNotation.hide();
						}, function(){
							$scope.modalNotation.hide();
							$rootScope.Utils.showToast('Un problème est survenu lors de la validation de la note. Veuillez réessayer.', 'Erreur');
						});
					} else{
						$scope.modalNotation.hide();
						$rootScope.Utils.showNetworkErrorMsg();
					}
				};
			}
		]);