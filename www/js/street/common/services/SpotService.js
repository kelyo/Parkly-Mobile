/**
 * Created by kHaLiL on 07/04/2016.
 */

var parkly = parkly || {};
parkly.modules = parkly.modules || {};
parkly.modules.services = parkly.modules.services || angular.module('parkly.services', []);

// Création du service
parkly.modules.services
		.factory('SpotService', ['$http', '$q', 'APIBaseURL', '$localStorage', function ($http, $q, APIBaseURL, $localStorage) {
			return {
				getSpots: function(lat, lng){
					var deferred = $q.defer();

					$http({
						method: 'get',
						url: APIBaseURL + '/spots',
						params: {latitude: lat, longitude: lng}
					}).then(function successCallback(response){
						deferred.resolve(response.data);
					}, function errorCallback(error){
						deferred.reject(error);
					});

					return deferred.promise;
				},

				getParkingsPublics: function(lat, lng, ville, departement, region){
					var deferred = $q.defer();

					$http({
						method: 'get',
						url: APIBaseURL + '/parkingsPublics',
						params: {latitude: lat, longitude:lng, ville:ville, departement:departement, region:region}
					}).then(function successCallback(response){
						deferred.resolve(response.data);
					}, function errorCallback(error){
						deferred.reject(error);
					});

					return deferred.promise;
				},

				postSpot: function(spot){
					var deferred = $q.defer();

					$http({
						method: 'POST',
						url: APIBaseURL + '/spots',
						data: spot
					}).then(function successCallback(response){
						var data = response.data;

						if(data.success) {
							$localStorage.set('_st', (data.spot.end - parseInt(($localStorage.getInt('time-offset', 0)))) * 1000);
							$localStorage.setObject('_st_spot', JSON.stringify(data.spot));
							$localStorage.set('_st_ttl', Math.floor((((data.spot.end - parseInt(($localStorage.getInt('time-offset', 0)))) * 1000) - (new Date())) / 1000));

							deferred.resolve(response.data);
						} else{
							deferred.reject('Une erreur est survenue lors de l\'enregistrement du parking');
						}
					}, function errorCallback(error){
						deferred.reject(error);
					});

					return deferred.promise;
				},

				modifySpot: function(spot){
					var deferred = $q.defer();

					$http({
						method: 'PUT',
						data: { spot: spot },
						url: APIBaseURL + '/spots/'+ spot.id
					}).then(function successCallback(response) {
						deferred.resolve(response);
					}, function errorCallback(error){
						deferred.reject(error);
					});

					return deferred.promise;
				},

				deleteSpot: function(){
					var deferred = $q.defer();

					if($localStorage.getObject('_st_spot', null)) {
						$http({
							method: 'DELETE',
							url: APIBaseURL + '/spots/' + JSON.parse($localStorage.getObject('_st_spot')).id
						}).then(function successCallback(response) {
							deferred.resolve();
						}, function errorCallback(error) {
							deferred.reject(error);
						});
					} else{
						deferred.reject('Spot déjà supprimé');
					}

					return deferred.promise;
				}
			};
		}]);