/**
 * Created by kHaLiL on 09/03/2016.
 */
// Récupération de l'application existante ou création si nécessaire
var parkly = parkly || {};
// Récupération des modules de l'application existante ou création si nécessaire
parkly.modules = parkly.modules || {};
// Récupération du module controller de l'application existante ou création si nécessaire
parkly.modules.controllers = parkly.modules.controllers || angular.module('parkly.controllers', []);

// Création du controleur pour la vue Login
parkly.modules.controllers
		.controller('PasswordController', ['$scope', '$state', '$ionicPopover', '$ionicHistory', '$rootScope', '$ionicLoading',
					'AuthenticationService', '$cordovaDialogs', '$log',
			function ($scope, $state, $ionicPopover, $ionicHistory, $rootScope, $ionicLoading,
					  AuthenticationService, $cordovaDialogs, $log) {

			$scope.loginData = {
				email: ''
			};
			//TODO : A supprimer
			/*$scope.loginData = {
				email: 'khalil_simo@hotmail.fr'
			};*/

			$rootScope.title = 'Mot de passe';

			$scope.buttonDisabled = function(){
				//return $scope.loginData.email.length < 3;
				var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
				return re.test($scope.loginData.email);
			};

			$scope.reinitPassword = function(){
				$log.debug('Password : handling password reset...');

				$ionicLoading.show();
				var userData = {
					email: $scope.loginData.email
				};

				if($rootScope.networkAvailable) {
					AuthenticationService.resetPassword(userData).then(function (response) {
						$ionicLoading.hide();
						$scope.loginData.email = '';
						$cordovaDialogs.alert('Un email vous a été envoyé avec un lien de réinitialisation de mot de passe.', 'Réinitialiser le mot de passe', 'OK')
								.then(function () {
									$ionicHistory.nextViewOptions({
										disableBack: true
									});
									$state.go('welcome');
								});
					}).catch(function (error) {
						$ionicLoading.hide();
						$cordovaDialogs.alert(error, 'Erreur de réinitialisation', 'OK');
					});
				} else{
					$rootScope.Utils.showNetworkErrorMsg();
				}
			};

		}]);
