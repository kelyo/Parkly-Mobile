/**
 * Created by kHaLiL on 09/03/2016.
 */
// Récupération de l'application existante ou création si nécessaire
var parkly = parkly || {};
// Récupération des modules de l'application existante ou création si nécessaire
parkly.modules = parkly.modules || {};
// Récupération du module controller de l'application existante ou création si nécessaire
parkly.modules.controllers = parkly.modules.controllers || angular.module('parkly.controllers', []);

// Création du controleur pour la vue Login
parkly.modules.controllers
		.controller('WelcomeController', ['$scope', '$rootScope', '$state', '$ionicPopover', '$auth', '$ionicLoading', 'AuthenticationService',
			'$log', '$cordovaDialogs', '$ionicHistory', '$ionicModal',
			function ($scope, $rootScope, $state, $ionicPopover, $auth, $ionicLoading, AuthenticationService,
					  $log, $cordovaDialogs, $ionicHistory, $ionicModal) {

				$scope.loginData = {};
			$scope.$on('$ionicView.beforeEnter', function () {
				if(typeof $rootScope.stopGetSpots !== 'undefined') {$rootScope.stopGetSpots();}
				if(typeof $rootScope.map !== 'undefined') {$rootScope.map.clear();}

				//if ($auth.isAuthenticated()) {$state.go('app.je-cherche');}
			});

			$rootScope.title = 'Inscription / Connexion';

			$scope.goToLogin = function () {
				$state.go('login', {'context': 'login'});
			};
			$scope.goToRegister = function () {
				$state.go('login', {'context': 'register'});
			};
				
			// Active le bouton valider (adresse email) si l'adresse saisie est valide
			$scope.buttonDisabled = function(){
				var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
				//var re = /^([a-zA-Z]+)([0-9]*)@([a-zA-Z]+)([0-9]*).[a-zA-Z]{2,}$/;
				return !re.test($scope.profileInfo.email);
			};


			/*********************************************************
			 * FACEBOOK LOGIN
			 *
			 * Plugin à installer :
			 *     	https://github.com/jeduan/cordova-plugin-facebook4
			 * Tuto :
			 * 		https://ionicthemes.com/tutorials/about/native-facebook-login-with-ionic-framework
			 *
			 ********************************************************/
			$scope.fbLoginFinalize = function(){
				if($scope.modal){
					$scope.modal.hide();
				}
				AuthenticationService.fbLogin($scope.profileInfo).then(function(user){
					$ionicHistory.nextViewOptions({
						disableBack: true
					});

					$ionicLoading.hide();

					AuthenticationService.goToHomeOrUserCar(user);
				}).catch(function(){
					$ionicLoading.hide();
					$rootScope.Utils.showToast('Une erreur est survenue lors de la connexion Facebook.');
				});
			};


			// This is the success callback from the login method
			var fbLoginSuccess = function(response) {
				if (!response.authResponse){
					fbLoginError({errorMessage : 'Impossible de se connecter avec Facebook. Réessayez ultérieurement'});
					return;
				}

				AuthenticationService.getFacebookProfileInfo(response.authResponse).then(function(profileInfo) {
					$scope.profileInfo = {};
					$scope.profileInfo.authResponse = response.authResponse;
					$scope.profileInfo.userID = profileInfo.id;
					$scope.profileInfo.name = profileInfo.name;
					$scope.profileInfo.first_name = profileInfo.first_name;
					$scope.profileInfo.last_name = profileInfo.last_name;
					$scope.profileInfo.sex = profileInfo.gender === 'male' ? 'm':'f';
					$scope.profileInfo.picture = 'http://graph.facebook.com/' + response.authResponse.userID + '/picture?type=large';
					
					if(typeof profileInfo.email === 'undefined' || !profileInfo.email){
						$ionicModal.fromTemplateUrl('auth/modal-email.html', {
							scope: $scope,
							animation: 'slide-in-up',
							focusFirstInput: true,
							backdropClickToClose: false,
							hardwareBackButtonClose: false
						}).then(function(modal) {
							$ionicLoading.hide();
							$scope.modal = modal;
							$scope.modal.show();
						});
					} else {
						$scope.profileInfo.email = profileInfo.email;
						$scope.fbLoginFinalize();
					}
				}, function(fail){
					$ionicLoading.hide();
					var msg = 'Vous avez désactivé l\'accès à votre adresse email, et nous comprenons cette décision.\n\rCela dit, l\'adresse email constitue votre identifiant unique et est nécessaire pour l\'utilisation de Parkly.';
					var title = 'Adresse email';

					$cordovaDialogs.confirm(msg, title, ['Réessayer', 'Annuler']).then(function(buttonIndex){
						if(buttonIndex === 1){ //Réessayer
							facebookConnectPlugin.login(['email', 'public_profile'], fbLoginSuccess, fbLoginError);
						} else if(buttonIndex === 2){ //Annuler
							$log.warn('FBConnect : profile info fail', fail);
							$ionicLoading.hide();
							$rootScope.Utils.showToast('L\'authentification à l\'aide de votre compte Facebook n\'a pas été possible.');
						}
					});
				});
			};

			// This is the fail callback from the login method
			var fbLoginError = function(error){
				$rootScope.Utils.showToast(error.errorMessage, 'Erreur Facebook');
				$ionicLoading.hide();
			};
			
			//This method is executed when the user press the "Login with facebook" button
			$scope.facebookSignIn = function() {
				$ionicLoading.show();

				facebookConnectPlugin.getLoginStatus(function(response){
					if(response.status === 'connected'){
						// The user is logged in and has authenticated your app, and response.authResponse supplies
						// the user's ID, a valid access token, a signed request, and the time the access token
						// and signed request each expire
						$log.debug('FBConnect : getLoginStatus', response.status);
						
						// Check if we have our user saved
						var user = AuthenticationService.getLocalUser('facebook');
						
						if(!user || !user.fb_user_id){
							fbLoginSuccess(response);
						}else{
							$state.go('app.je-cherche');
						}
					} else {
						// If (response.status === 'not_authorized') the user is logged in to Facebook,
						// but has not authenticated your app
						// Else the person is not logged into Facebook,
						// so we're not sure if they are logged into this app or not.
						
						$log.debug('FBConnect : getLoginStatus', response.status);
						
						// Ask the permissions you need. You can learn more about
						// FB permissions here: https://developers.facebook.com/docs/facebook-login/permissions/v2.4
						facebookConnectPlugin.login(['email', 'public_profile'], fbLoginSuccess, fbLoginError);
					}
				});
			};



			/*********************************************************
			 * GOOGLE LOGIN
			 *
			 * Plugin à installer :
			 *     	cordova plugin add cordova-plugin-googleplus --variable REVERSED_CLIENT_ID=myreversedclientid
			 * Tuto :
			 * 		https://ionicthemes.com/tutorials/about/google-plus-login-with-ionic-framework
			 *
			 ********************************************************/

			//This method is executed when the user press the "Login with facebook" button
			$scope.googleSignIn = function() {
				$ionicLoading.show();

				window.plugins.googleplus.login(
						{},
						function (user_data) {
							$log.debug('Google Login -> userData : ', user_data);

							AuthenticationService.googleLogin({
								userID: user_data.userId,
								name: user_data.displayName,
								first_name: user_data.givenName,
								last_name: user_data.familyName,
								email: user_data.email,
								picture : user_data.imageUrl,
								serverAuthCode: user_data.serverAuthCode,
								idToken: user_data.idToken
							}).then(function(user){
								$ionicHistory.nextViewOptions({
									disableBack: true
								});

								$ionicLoading.hide();

								AuthenticationService.goToHomeOrUserCar(user);
							}).catch(function(){
								$ionicLoading.hide();
								$rootScope.Utils.showToast('Une erreur est survenue lors de la connexion Google.', 'Erreur Google');
							});
						}, function(fail){
							// Fail get profile info
							$log.debug('GoogleConnect : profile info fail', fail);
							$ionicLoading.hide();

							$rootScope.Utils.showToast('Une erreur est survenue lors de la connexion Google.', 'Erreur Google');
						}
				);
			};

		}]);
